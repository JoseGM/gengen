package main

import (
	"database/sql"
	"flag"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"math/rand"
	"strconv"
	"time"
)

type Subject struct {
	id               int64
	identifier, name string
}

type Genotype struct {
	id         int64
	subject_id int
}

type MarkerResult struct {
	id                       int64
	genotype_id              int64
	marker, allele1, allele2 string
}

func main() {
	fmt.Println("Genotype generator by Logica2")
	// command-line args (flags)
	gens := flag.Int("g", 10, "number of genotypes to insert")
	threads := flag.Int("t", 4, "number of concurrent inserts")
	dbName := flag.String("d", "altea_test", "Altea database name")
	flag.Parse()

	db, err := sql.Open("postgres", "host=localhost dbname="+*dbName+" sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	rand.Seed(time.Now().UTC().UnixNano())

	var startTime, endTime time.Time
	startTime = time.Now()
	c := make(chan int)
	var split_range = *gens / *threads
	for current_split := 0; current_split < *threads; current_split++ {
		go launchRange(db, current_split*split_range, split_range, c)
	}

	cont := 0
	go_ended := 0
	for i := range c {
		if i < 0 {
			go_ended++
			if go_ended > *threads-1 {
				break
			}
		}
		cont += i
		if cont%100 == 0 {
			fmt.Printf("%d\r", cont)
		}
	}

	endTime = time.Now()
	fmt.Printf("Duration: %v\n", endTime.Sub(startTime))
}

func launchRange(db *sql.DB, start_range int, range_count int, c chan int) {
	max := start_range + range_count
	var startTime, endTime time.Time
	startTime = time.Now()
	for i := start_range; i < max; i++ {
		getSubject(db, i)
		c <- 1
	}
	endTime = time.Now()
	fmt.Printf("From: %d to %d\n", start_range, max)
	fmt.Printf("Duration: %v\n", endTime.Sub(startTime))
	c <- -1
}

func getSubject(db *sql.DB, base_id int) Subject {
	// identifier and name
	subject := Subject{identifier: "id" + strconv.Itoa(base_id),
		name: "name" + strconv.Itoa(base_id)}
	subject.id = insertSubject(db, subject)

	getGenotype(db, subject.id)
	return subject
}

func insertSubject(db *sql.DB, subject Subject) int64 {
	var id int64
	var sStmt string = "insert into subjects (identifier, name, specie_id, created_at, updated_at) values ($1, $2, 1, now(), now()) RETURNING id"
	err := db.QueryRow(sStmt, subject.identifier, subject.name).Scan(&id)
	if err != nil {
		log.Fatal(err)
	}
	return id
}

func getGenotype(db *sql.DB, subject_id int64) {
	gen := Genotype{}
	gen.id = insertGenotype(db, subject_id)
	getMarkerResults(db, gen.id)
}

func insertGenotype(db *sql.DB, subject_id int64) int64 {
	var id int64
	var sStmt string = "insert into genotypes (specie_id, subject_id, is_master, created_at, updated_at) values (1, $1, true, now(), now()) RETURNING id"
	err := db.QueryRow(sStmt, subject_id).Scan(&id)
	if err != nil {
		log.Fatal(err)
	}
	return id
}

var markers []string = []string{"csf1po", "d5s818", "d7s820", "d13s317", "tpox", "d3s1358", "d8s1179", "d16s539", "d18s51", "d21s11", "fga", "th01", "vwa", "d2s1338", "d19s433", "amel"}
var amelAlleles []string = []string{"X", "Y"}

func getMarkerResults(db *sql.DB, genotype_id int64) {
	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	for _, marker := range markers {
		mr := MarkerResult{marker: marker, genotype_id: genotype_id}
		if marker != "amel" {
			mr.allele1 = strconv.Itoa(rand.Intn(32))
			mr.allele2 = strconv.Itoa(rand.Intn(32))
		} else {
			mr.allele1 = "X"
			mr.allele2 = amelAlleles[rand.Intn(2)]
		}
		insertMarkerResult(tx, mr)
	}
	tx.Commit()
}

func insertMarkerResult(db *sql.Tx, mr MarkerResult) {
	var sStmt string = "insert into marker_results (genotype_id, marker, allele1, allele2) values ($1, $2, $3, $4)"
	_, err := db.Exec(sStmt, mr.genotype_id, mr.marker, mr.allele1, mr.allele2)
	if err != nil {
		log.Fatal(err)
	}
}
